const path = require('path');
const express = require('express');
const compression = require('compression');
const mongoose = require('mongoose');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const employeeRouter = require('./components/employees/employee.route')
const managerRouter = require('./components/managers/manager.route')


const config = require('./config');

mongoose.connect(config.db, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });  
//mongoose.Promise = global.Promise;

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', function() {
    console.log('You connected to db!!!')
});

const DIST_APP_FOLDER = path.join(__dirname, config.appFolder);

const app = express();
app.use(compression());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Credetinals", "true");
    res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE , OPTIONS");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept , Authorization");
    next();
});

app.use(morgan('dev'));


app.use('/api/employee', employeeRouter);
app.use('/api/manager', managerRouter); 

app.use(express.static(DIST_APP_FOLDER));
app.get('/', function(req, res) {
    res.sendFile(path.join(DIST_APP_FOLDER, 'index.html'));
});


app.use((req, res, next) => {
    const error = new Error("Request not found");
    error.status = 404;
    next(error);
}); 

app.use((error, req, res, next) => {
    res.status(error.status || 500); 
    res.json({
        error: {
            message: error.message
        }
    }); 
});

const server = app.listen(config.port, () =>
{
    console.log(`server listening on port ${config.port}`);
});

