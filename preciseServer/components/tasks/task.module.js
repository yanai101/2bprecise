const mongoose = require('mongoose');

const taskSchema = mongoose.Schema({
    text:String,
    assingDate: {type: Date, default: Date.now },
    dueDate: Date
}, { timestamps: true });

module.exports = mongoose.model('Task', taskSchema);