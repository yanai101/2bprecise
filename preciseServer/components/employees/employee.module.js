const mongoose = require('mongoose');

const employeeSchema = mongoose.Schema({
    position: {type: String},
    firstName: { type: String, require: true },
    lastName: { type: String, require: true }, 
    leader: {type:  mongoose.Schema.Types.ObjectId, ref: 'Manager'},
    reports:[{type: mongoose.Schema.Types.ObjectId, ref:'Report'}],
    tasks:[{type: mongoose.Schema.ObjectId , ref:'Task'}]
}, { timestamps: true });

module.exports = mongoose.model('Employee', employeeSchema);