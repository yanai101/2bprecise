const express = require('express');
let router = express.Router();

const employeeActions = require('./employee.actions');

router.route('/')
    .post(employeeActions.add_employee)
    .get(employeeActions.get_emploeey_list);

router.route('/report')  
    .post(employeeActions.add_emploeey_report);

router.route('/:id')  
    .get(employeeActions.get_emploeey)

module.exports = router;