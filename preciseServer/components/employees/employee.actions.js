const mongo = require('mongodb');
const mongoose = require('mongoose');
const EmployeeModule = require('./employee.module');
const reposrtAction = require('../reports/report.actions');
const ManagerModule = require('../managers/managers.module');


exports.add_employee = async (req, res, next) => {
    try{
        const {body} = req;
        const employeeExist = await EmployeeModule.find({firstName: body.firstName, lsatName: body.lsatName}).exec();
        
        if(!employeeExist.length){
            const newEmploeey = await EmployeeModule.create({...body});
            if(newEmploeey){
                res.json(newEmploeey);
            }else{
                    res.status(500).json({msg:`Sorry something went wrong`});   
            }
        } else {
            res.status(400).json({msg:`emploeey- ${body.firstName} already exist`}); 
        } 

    }catch(error){
        res.sendStatus(500).json(error);
        res.end() 
    }
}


exports.get_emploeey_list = async (req, res, next) => {
    try{
        const emploeeysList = await EmployeeModule.find({}).populate('leader','firstName lastName id').exec(); //.select('formName _id')
        
        if(emploeeysList.length){
            res.json(emploeeysList);
        } else {
            res.status(400).json({msg:`You don't have any employee to display`});
        } 

    }catch(error){
        res.sendStatus(500).json(error);
        res.end() 
    }
}

exports.get_emploeey = async (req, res, next) =>{
    const {id} = req.params;

    try{
        const emploeey = await EmployeeModule.find({_id: id})
                            .populate('leader','firstName lastName id')
                            .populate('tasks')
                            .populate('reports')
                            .exec(); 
        
        if(emploeey){
            res.json(emploeey);
        } else {
            res.status(400).json({msg:`You don't have any employee to display`});
        } 

    }catch(error){
        console.log(error)
        res.sendStatus(500).json(error);
        res.end() 
    }
}



exports.add_emploeey_report = async (req, res, next) => {
    const {text , employeeId , managerId} = req.body;
    try{
    
        const report = await reposrtAction.add_report(text);
        const employeeUpdate = await EmployeeModule.findOneAndUpdate({_id: employeeId},{ "$push": { "reports": report._id }}).exec();
        const managerUpdate = await ManagerModule.findOneAndUpdate({_id: managerId },{ "$push": { "reports": report._id }}).exec();

        if(employeeUpdate && managerUpdate){
            res.json({msg:'Reposrt had been added '});
        } else {
            res.status(400).json({msg:`Error happen`});
        } 

    }catch(error){
        console.log(error)
        res.sendStatus(500).json(error);
        res.end() 
    }
}


// exports.get_form = async (req, res, next) => {
//     const {id} = req.params;
//     try{
//         const form = await forms.findById(id).select('_id formName fields').exec();
//         if(form){
//             res.json(form);
//         } else {
//            res.status(400).json({msg:`Form id is invalid`});
//         } 

//     }catch(error){
//         res.sendStatus(500).json(error);
//     }
// }