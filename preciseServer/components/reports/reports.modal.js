const mongoose = require('mongoose');

const reportSchema = mongoose.Schema({
    text:String
}, { timestamps: true });

module.exports = mongoose.model('Report', reportSchema);