const express = require('express');
let router = express.Router();

const managerActions = require('./managers.actions');

router.route('/')
    .post(managerActions.add_manager)
    .put(managerActions.update_manager_subordinates)
    .get(managerActions.get_manager_list);

router.route('/:id')   
    .get(managerActions.get_manager)    

router.route('/task')  
    .post(managerActions.add_manager_task)

module.exports = router;