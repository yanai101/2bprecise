const mongo = require('mongodb');
const mongoose = require('mongoose');
const ManagerModule = require('./managers.module');
const EmployeeModule = require('../employees/employee.module');
const taskActions = require('../tasks/tasks.actions');


exports.add_manager = async (req, res, next) => {
    try{
        const {body} = req;
        const managerExist = await ManagerModule.find({firstName: body.firstName, lsatName: body.lsatName}).exec();
        
        if(!managerExist.length){
            const newManager = await ManagerModule.create({...body});
            if(newManager){
                res.json(newManager);
            }else{
                res.status(500).json({msg:`Sorry something went wrong`});   
            }
        } else {
            res.status(400).json({msg:`manager- ${body.firstName} already exist`});
        } 

    }catch(error){
        console.log(error)
        res.sendStatus(500).json(error);
        res.end() 
    }
}

exports.update_manager_subordinates = async (req, res, next) => {
    const {managerId: id , employeeId } = req.body;
    try{
        const managerUpdate = await ManagerModule.findOneAndUpdate({_id: id},{ "$push": { "subordinates": employeeId }}).exec(); //.select('formName _id')
        
        if(managerUpdate){
            res.json(managerUpdate);
        } else {
            res.status(400).json({msg:`Error happen while update managedr subordinates`});
        } 

    }catch(error){
        res.sendStatus(500).json(error);
        res.end() 
    }
}


exports.get_manager_list = async (req, res, next) => {
    try{
        const managersList = await ManagerModule.find({}).exec(); //.select('formName _id')
        
        if(managersList.length){
            res.json(managersList);
        } else {
            res.status(400).json({msg:`You don't have any employee to display`});
        } 

    }catch(error){
        res.sendStatus(500).json(error);
        res.end() 
    }
}


exports.get_manager = async (req, res, next)=>{
    const {id} = req.params;
    try{
        const managersList = await ManagerModule.find({_id: id})
                                    .populate('subordinates','firstName lastName id')
                                    .populate('reports')
                                    .exec(); //.select('formName _id')
        
        if(managersList.length){
            res.json(managersList);
        } else {
            res.status(400).json({msg:`You don't have any employee to display`});
        } 

    }catch(error){
        console.log(error)
        res.sendStatus(500).json(error);
        res.end() 
    }
}

exports.add_manager_task = async (req, res, next) => {
    const {text , assingDate = Date.now() , dueDate ,  employeeId , managerId} = req.body;
    try{
        const task = await taskActions.add_task(text, dueDate , assingDate);
        const employeeUpdate = await EmployeeModule.findOneAndUpdate({_id: employeeId},{ "$push": { "tasks": task._id }}).exec();
        const managerUpdate = await ManagerModule.findOneAndUpdate({_id: managerId },{ "$push": { "tasks": task._id }}).exec();
        
        if(employeeUpdate && managerUpdate){
            res.json({msg:`task had been added`});
        } else {
            res.status(400).json({msg:`Error happen`});
        } 

    }catch(error){
        console.log(error)
        res.sendStatus(500).json(error);
        res.end() 
    }
}