const mongoose = require('mongoose');

const managerSchema = mongoose.Schema({
    firstName: { type: String, require: true },
    lastName: { type: String, require: true }, 
    subordinates:[{type: mongoose.Schema.ObjectId, ref: 'Employee'}],
    reports:[{type: mongoose.Schema.Types.ObjectId, ref:'Report'}],
    tasks:[{type: mongoose.Schema.ObjectId , ref:'Task'}]
}, { timestamps: true });

module.exports = mongoose.model('Manager', managerSchema);