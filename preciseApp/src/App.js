import React from 'react';
import Header from './components/layoutComponents/header'
import style from './app.module.scss';
import {  BrowserRouter as Router , Route, Switch } from 'react-router-dom';
import Notfound from './pages/notfound/Notfound';
import { createBrowserHistory } from "history";
import EmployeeList from './pages/employeeList/employeeList';
import HomePage from './pages/home/homePage';
import ManagerList from './pages/managerList/managerList';
import EmployeePage from './pages/employeePage/employeePage';
import ManagerPage from './pages/managerPage/managerPage';

const history = createBrowserHistory();


function App() {
  return (
    <main className={style.AppContainer}>
      <Router history={history}>
        <Header/>  
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/employees" component={EmployeeList} />
          <Route exact path="/employee/:id" component={EmployeePage} />
          <Route exact path="/manager" component={ManagerList} />
          <Route exact path="/manager/:id" component={ManagerPage} />
          <Route component={Notfound} />
        </Switch>
      </Router >  
    </main>
  );
}

export default App;
