import React, { useEffect, useState } from 'react';
import {  STAFF_TYPE } from '../../config/config';
import Alert from 'react-bootstrap/Alert';
import { getEmployee } from '../../utils/apiManager';
import ListGroup from 'react-bootstrap/ListGroup';
import StaffHeader from '../../components/uiComponent/staffHeader/staffHeader';

import './employeePage.scss';

export default function EmployeePage({match}){
    const [loading , setLoading]= useState(true);
    const [employee , setEmployee]= useState(null);
    const [loadingError , setLoadingError]= useState(false);

    useEffect(()=>{
        getEmployee(match.params.id , setEmployee , (msg)=> setLoadingError(msg), ()=>{
            setLoading(false)
        })
    },[match.params.id])

    return(
        <div>
            <h2>Employee page</h2>

            {loading && 'loading' }
            { employee && 
            <div>
                <StaffHeader staff={employee} staffType={STAFF_TYPE.EMPLOYEE}/>
                <h3 className="tasks">My tasks</h3>
                <ListGroup>
                    {employee.tasks.length && employee.tasks.map(task=> <ListGroup.Item key={task._id}>{task.text}</ListGroup.Item>)}
                </ListGroup>
            </div>    
            }

            {loadingError && <Alert variant="warning">{loadingError}</Alert>}
        </div>
    )
}
