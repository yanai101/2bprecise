import React, { useEffect, useState } from 'react';
import {  STAFF_TYPE } from '../../config/config';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';
import { getManagers } from '../../utils/apiManager';
import StaffList from '../../components/uiComponent/staffList/staffList';

export default function ManagerList(){
    
    const [manager ,setManager] = useState([]);
    const [loading ,setLoading] = useState(true);
    const [loadingError, setLodgingError] = useState();

    useEffect(()=>{
        getManagerList();

    },[])

    function getManagerList(){
        getManagers(
            (data)=>{
                setManager(data);
                setLoading(false);
            },
            (error)=>{
                setLodgingError(error.msg);
                setLoading(false);
            }
        )
    }

    return(
        <div>
            <h2>{loading ? <span><Spinner animation="grow" />loading...</span> :'Manager list'}</h2>
            <StaffList staff={manager} staffType={STAFF_TYPE.MANAGER}/>
            {manager.length === 0 && <Alert variant="info">{loadingError ? loadingError : 'There is no Manager yet'}</Alert>}
        </div>
    )
}
