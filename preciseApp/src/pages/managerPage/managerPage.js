import React, { useEffect, useState } from 'react';
import {  STAFF_TYPE } from '../../config/config';
import Alert from 'react-bootstrap/Alert';
import { getManager } from '../../utils/apiManager';
import ListGroup from 'react-bootstrap/ListGroup';
import Toast from 'react-bootstrap/Toast';
import StaffHeader from '../../components/uiComponent/staffHeader/staffHeader';

import './managerPage.scss';
import ManagerTaskAssing from '../../components/uiComponent/managerTaskAssing/managerTaskAssing';

export default function ManagerPage({match}){
    const [loading , setLoading]= useState(true);
    const [manager , setManager]= useState(null);
    const [loadingError , setLoadingError]= useState(false);
    const [showToast , setShowToast] = useState(false);

    useEffect(()=>{
        getManager(match.params.id , setManager , (msg)=> setLoadingError(msg), ()=>{
            setLoading(false)
        })
    },[match.params.id])

    function toggleToast(){
        setShowToast(true);
    }

    return(
        <div>
            <h2>Manager page</h2>

            {loading && 'loading' }
            { manager && 
            <div>
                <Toast onClose={() => setShowToast(false)} show={showToast} delay={3000} autohide>
                    <Toast.Body>Task added!</Toast.Body>
                </Toast>
                <StaffHeader staff={manager} staffType={STAFF_TYPE.MANAGER}/>
                <h3 className="tasks">My Reports</h3>
                <ListGroup>
                    {manager.reports.length && manager.reports.map(report=> <ListGroup.Item key={report._id}>{report.text}</ListGroup.Item>)}
                </ListGroup>

                <h3 className="tasks">My subordinates</h3>
                <ListGroup>
                    {manager.subordinates.length && manager.subordinates.map(sub=> 
                        <ListGroup.Item key={sub._id}>{sub.firstName} {sub.lastName} 
                            <span className="addTask">
                                <ManagerTaskAssing cbFunction={toggleToast} key={sub._id} managerId={manager._id} subId={sub._id}/> 
                            </span>
                        </ListGroup.Item>)
                    }
                </ListGroup>
            </div>    
            }

            {loadingError && <Alert variant="warning">{loadingError}</Alert>}
        </div>
    )
}
