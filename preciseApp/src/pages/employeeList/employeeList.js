import React, { useEffect, useState } from 'react';
import { STAFF_TYPE } from '../../config/config';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';
import { getEmployeeList } from '../../utils/apiManager';
import StaffList from '../../components/uiComponent/staffList/staffList';

export default function EmployeeList(){
    
    const [employees ,setEmployees] = useState([]);
    const [loading ,setLoading] = useState(true);
    const [loadingError, setLodgingError] = useState();

    useEffect(()=>{
        getEmployeesList();
    },[])

    function getEmployeesList(){
        getEmployeeList(
            (data)=>{
                setEmployees(data);
                setLoading(false);
            },
            (error)=>{
                setLodgingError(error.msg);
                setLoading(false);
            }
        )
    }

    return(
        <div>
            <h2>{loading ? <span><Spinner animation="grow" />loading...</span> :'Employee list'}</h2>
            {employees.length === 0 && <StaffList staffType={STAFF_TYPE.EMPLOYEE} staff={employees}/>}
        </div>
    )
}
