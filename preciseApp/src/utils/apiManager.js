import { API_URL } from "../config/config";

export async function getManagers(successCb , errorCb){
        const response = await fetch(`${API_URL}/manager`).catch(e=> errorCb(e));
        const data = await response.json();
        response.status === 200 ?  successCb(data) : errorCb(data.msg);
}


export async function addManager(newManager ,successCb , errorCb){
    const response = await fetch(`${API_URL}/manager`,
    {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(newManager)
    }).catch(e=> {
        errorCb(e)
    });
    const data = await response.json();
    response.status === 200 ?  successCb(data) : errorCb(data.msg);
}


export async function addEmployee(newEmployee,successCb , errorCb){
    const response = await fetch(`${API_URL}/employee`,
    {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(newEmployee)
    }).catch(e=> {
        errorCb(e)
    });
    const data = await response.json();
    response.status === 200 ?  successCb(data) : errorCb(data.msg);
}

export async function getEmployeeList(successCb , errorCb){
        const response = await fetch(`${API_URL}/employee`).catch(e=>errorCb(e));
        const data = await response.json();
        response.status === 200 ?  successCb(data) : errorCb(data.msg);
}


export async function getEmployee(employeeId , successCb , errorCb , finallyCb){
    const response = await fetch(`${API_URL}/employee/${employeeId}`).catch(e=>errorCb(e));
    const data = await response.json();
    response.status === 200 ?  successCb(data[0]) : errorCb(data.msg);
    finallyCb();
}



export async function getManager(managerId , successCb , errorCb , finallyCb){
    const response = await fetch(`${API_URL}/manager/${managerId}`).catch(e=>errorCb(e));
    const data = await response.json();
    response.status === 200 ?  successCb(data[0]) : errorCb(data.msg);
    finallyCb();
}

export async function addReport(reportData , successCb , errorCb , finallyCb){
    const response = await fetch(`${API_URL}/employee/report`,
    {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(reportData)
    }).catch(e=> {
        errorCb(e)
    });
    const data = await response.json();
    response.status === 200 ?  successCb(data) : errorCb(data.msg);
}


export async function addTask(task , successCb , errorCb , finallyCb){
    const response = await fetch(`${API_URL}/manager/task`,
    {
        method: 'post',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(task)
    }).catch(e=> {
        errorCb(e)
    });
    const data = await response.json();
    response.status === 200 ?  successCb(data) : errorCb(data.msg);
}