export const API_URL = 'http://localhost:8081/api';

export const STAFF_TYPE = {
    EMPLOYEE: 'employee',
    MANAGER: 'manager'
}

export const MISSION_TYPE = {
    REPORT: 'report',
    TASK: 'task'
}