import React, { useState } from 'react';
import { MISSION_TYPE } from '../../../config/config';
import Button from 'react-bootstrap/Button';
import TaskRepoertModal from '../taskReportModal/taskReportModal';




export default function ManagerTaskAssing({managerId , subId , cbFunction}){
const [showMoadl , setShowModal] = useState(false);


function toggleModal(){
    setShowModal(!showMoadl)
}

function closeModal(){
    toggleModal();
    cbFunction();
}

return(
    <>
        <Button variant="info" onClick={toggleModal}>Add task</Button>
        <TaskRepoertModal
        show={showMoadl} 
        type={MISSION_TYPE.TASK} 
        cbFunction={closeModal} 
        closeModal={toggleModal} 
        employeeId={subId} 
        managerId={managerId}/>
    </>   
    )
}