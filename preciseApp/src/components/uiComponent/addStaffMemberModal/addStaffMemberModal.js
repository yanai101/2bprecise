import React, { useState, useRef } from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import './formBuilderModal.scss'
import { useEffect } from 'react';
import { STAFF_TYPE } from '../../../config/config';
import { getManagers, addEmployee, addManager } from '../../../utils/apiManager';


export default function AddStaffMemberModal({show, staffType , toggleModal}) {

  const [formError, setError] = useState('');
  const [managerList, setManagerList] = useState('');
  const managerInput = useRef(null);
  const nameInput = useRef(null);
  const lastNameInput = useRef(null);
  const positionInput = useRef(null);

  useEffect(()=>{
    if(staffType === STAFF_TYPE.EMPLOYEE){
      getManagers(setManagerList, setError)
    }
  },[staffType]);


  function resetFields(){
    managerInput.current.value = '';
    nameInput.current.value ='';
    lastNameInput.current.value = '';
    positionInput.current.value = '';
    setError('')
  }


  function checkFields(){
      let validForm = nameInput.current.value && lastNameInput.current.value ? true : false;
      if(staffType === STAFF_TYPE.EMPLOYEE && validForm){
        validForm = managerInput.current.value && positionInput.current.value ? true : false;
      }
      validForm ? addStaffMember() : setError('All fields are mandatory')
  }

  function addStaffMember(){
    let employeeFiled = {};
    if(staffType === STAFF_TYPE.EMPLOYEE){
      employeeFiled.leader = managerInput.current.value;
      employeeFiled.position = positionInput.current.value;
    }
    const newMember = {
      firstName: nameInput.current.value,
      lastName: lastNameInput.current.value,
      ...employeeFiled
    }

    staffType === STAFF_TYPE.EMPLOYEE ? addNewEmployee(newMember) : addNewManager(newMember);
  }

  function addNewManager(newMember){
    addManager(newMember, toggleModal , (e)=> {
      setError(e)
    });
  }

  function addNewEmployee(newMember){
    addEmployee(newMember, toggleModal , (e)=> {
      setError(e)
    });
  }


    return (
      <>  
        <Modal show={show} onHide={resetFields}>
          <Modal.Header>
            <Modal.Title>Add {staffType} </Modal.Title>
          </Modal.Header>
          <Modal.Body className='formBuilderModal'>
              {
                staffType === STAFF_TYPE.EMPLOYEE && managerList ?
                <p>
                  <label htmlFor="manager">Select Manager</label>
                  <select name="manager" id="manager" ref={managerInput}>
                      {
                          managerList.map((manager, index)=><option key={index} value={manager._id}>{manager.firstName} {manager.lastName}</option>)
                      }
                  </select>
                </p>:
                <Alert variant="danger">You have to insert manager first</Alert>
                }
                <p>
                  <label htmlFor="firstName">First name</label>
                  <input name="firstName" id="firstName" ref={nameInput}/>
                </p>
                <p>
                  <label htmlFor="lastName">Last name</label>
                  <input name="lastName" id="lastName" ref={lastNameInput}/>
                </p>
                {
                staffType === STAFF_TYPE.EMPLOYEE && managerList &&
                  <p>
                    <label htmlFor="position">position</label>
                    <input name="position" id="position" ref={positionInput}/>
                  </p>
                }
                <Alert variant="warning" show={ formError.length > 0}>{formError}</Alert>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={toggleModal}>
              Close
            </Button>
            <Button variant="primary" onClick={checkFields}>
              Add Filed
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
