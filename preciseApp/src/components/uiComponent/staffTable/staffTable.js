import React from 'react';
import {Link} from 'react-router-dom';
import Table from 'react-bootstrap/Table';
import { STAFF_TYPE } from '../../../config/config';


export default function StaffTable({staffList, staffType}){

const formsItems = staffList.map(({_id , firstName , lastName , position}) => (
                                <tr key={_id}>
                                    <td>{_id}</td>
                                    <td>{firstName}</td>
                                    <td>{lastName}</td>
                                    {staffType === STAFF_TYPE.EMPLOYEE && <td>{position}</td> }
                                    {staffType === STAFF_TYPE.EMPLOYEE && <td><Link to={`/employee/${_id}`}>View</Link></td>}
                                    {staffType === STAFF_TYPE.MANAGER && <td><Link to={`/manager/${_id}`}>View</Link></td>}
                                </tr>)
)    

return(
    <Table striped hover responsive> 
        <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last name</th>
                {staffType === STAFF_TYPE.EMPLOYEE && <th>Position</th>}
            </tr>
        </thead>
        <tbody>
            {formsItems}
        </tbody>
    </Table>
)

}