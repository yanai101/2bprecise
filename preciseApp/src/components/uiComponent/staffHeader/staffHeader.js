import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Image from 'react-bootstrap/Image';
import Toast from 'react-bootstrap/Toast';
import { STAFF_TYPE, MISSION_TYPE } from '../../../config/config';
import TaskRepoertModal from '../taskReportModal/taskReportModal';
import './staffHeader.scss';


export default function StaffHeader({staffType , staff}){
    const [modalOpen ,setModalOpen] = useState(false);
    const [show ,setShow] = useState(false);
 
    function toggleModal(){
        setModalOpen(!modalOpen);
    }

    function showToast(){
        toggleModal();
        setShow(true)
    }

    return(
        <>
            <Toast onClose={() => setShow(false)} show={show} delay={3000} autohide>
                <Toast.Body>Report added!</Toast.Body>
            </Toast>
        <div className="staffHeader">
            <Image src="https://i.pravatar.cc/150" rounded />
            <div className="staffDetails">
                <p><span>First Name</span> {staff.firstName}</p>
                <p><span>last Name</span>{staff.lastName}</p>
                <hr/>
                {
                    staffType === STAFF_TYPE.EMPLOYEE &&  <p><span>Manager</span> {staff.leader.firstName} {staff.leader.lastName}
                    <Button className="report" onClick={toggleModal}>Report</Button></p> 
                }
            </div>
                {
                staffType === STAFF_TYPE.EMPLOYEE && 
                <TaskRepoertModal 
                    show={modalOpen} 
                    type={MISSION_TYPE.REPORT} 
                    cbFunction={showToast} 
                    closeModal={toggleModal} 
                    employeeId={staff._id} 
                    managerId={staff.leader._id}/>
                }
        </div>
        </>
    )
}
