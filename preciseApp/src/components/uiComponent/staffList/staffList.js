import React, { useState } from 'react';
import StaffTable from '../staffTable/staffTable';
import Button from 'react-bootstrap/Button';
import AddStaffMemberModal from '../addStaffMemberModal/addStaffMemberModal';

export default function StaffList({staffType , staff}){
    const [modalOpen ,setModalOpen] = useState(false);

    function toggleModal(){
        setModalOpen(!modalOpen);
    }
    return(
        <div>
                <div>
                    <Button variant="info" onClick={toggleModal}>Add {staffType}</Button>
                    <AddStaffMemberModal show={modalOpen} staffType={staffType} toggleModal={toggleModal}/>
                    {staff && <StaffTable staffList={staff} staffType={staffType}/>}
                </div> 
        </div>
    )
}
