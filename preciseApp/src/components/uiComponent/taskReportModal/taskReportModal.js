import React ,{useRef , useState}  from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { MISSION_TYPE } from '../../../config/config';
import './taskReport.scss';
import { addReport, addTask } from '../../../utils/apiManager';
import Alert from 'react-bootstrap/Alert';


export default function TaskRepoertModal({show ,type , employeeId, managerId , cbFunction , closeModal}) {

  const [formError, setError] = useState('');
  const textInput = useRef(null);
  const assingDateInput = useRef(null);
  const dueDateInput = useRef(null);

  function checkFields(){
    let valid = textInput.current.value.length > 0 ;
    if(type === MISSION_TYPE.TASK){
      valid = dueDateInput.current.value && assingDateInput.current.value ? true : false;
    }
    return valid;
  }

  function submitForm(){
    const validForm = checkFields();
    if(!validForm){
      setError('All field are mandatory')
    }else{
      type === MISSION_TYPE.REPORT ? addEmployeeReport() : addManagerTask();
    }
  }

  function addEmployeeReport(){
    const reportData = {
      text : textInput.current.value , 
      employeeId , 
      managerId
    }

    addReport(reportData, hideModel , (e)=> setError(e))
  }

  function addManagerTask(){
    const taskData = {
      text : textInput.current.value , 
      employeeId , 
      managerId,
      assingDate: assingDateInput.current.value,
      dueDate: dueDateInput.current.value
    }
    addTask(taskData,  hideModel , (e)=>setError(e))
  }

  function hideModel(){
    cbFunction();
  } 
    
    return (
      <>  
        <Modal show={show} onHide={hideModel}>
          <Modal.Header>
            <Modal.Title>{MISSION_TYPE.REPORT === type ? 'Add Report': 'Add Task'} </Modal.Title>
          </Modal.Header>
          <Modal.Body>
                <p>
                  <label htmlFor="freeText">Taxt</label>
                  <input name="freeText" id="freeText" ref={textInput}/>
                </p>
                {
                  MISSION_TYPE.TASK === type && 
                  <>
                    <p>
                      <label htmlFor="assingDate">Assing date</label>
                      <input type="date" name="assingDate" id="assingDate" ref={assingDateInput}/>
                    </p>
                    <p>
                      <label htmlFor="dueDate">Due date</label>
                      <input type="date" name="dueDate" id="dueDate" ref={dueDateInput}/>
                    </p>
                  </>
                }
                {formError && <Alert variant="danger">{formError}</Alert>}
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={closeModal}>
              Close
            </Button>
            <Button variant="primary" onClick={submitForm}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>
      </>
    );
  }
